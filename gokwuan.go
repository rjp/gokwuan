package gokwuan

import (
    "time"
    "io"
    "os"
    "net"
    "fmt"
    "bytes"
    "sync"
    "strconv"
    "bitbucket.org/rjp/edf"
)

type ClientEvent struct {
    Event string
    Data  edf.EDF_Tree
    Extra interface{}
    Bot   *Client
}

type AbstractChan chan interface{}
type MapOfChannels map[string]AbstractChan
type MapOfStrings map[string]string

type Client struct {
    username string
    password string
    conn net.Conn
    Events chan ClientEvent
    toSocket chan string
    controlChan chan string
    wg *sync.WaitGroup
    ExtraWG *sync.WaitGroup
    Channels MapOfChannels
    ExtraData MapOfStrings
}

func (c *Client) net_out() {
    defer c.wg.Done()
    c.wg.Add(1)

    for {
        wantToSend := <-c.toSocket
        if wantToSend == "../quit" {
            return
        }
        _, e := c.conn.Write([]byte(wantToSend))

        if e != nil {
            fmt.Println(e)
            os.Exit(1)
        }
    }
}

func (c Client) net_in() {
    defer c.wg.Done()
    c.wg.Add(1)

    buffer := make([]byte, 131072)
    data := []byte{}


    for {
        // Set a 5s deadline
        c.conn.SetDeadline(time.Now().Add(5*time.Second))

        n, e := c.conn.Write([]byte{})
        if e != nil {
            fmt.Println(e)
            os.Exit(1)
        }
        n, e = c.conn.Read(buffer)
        if neterr, ok := e.(net.Error); ok && neterr.Timeout() {
            continue
        }

        if e == io.EOF {
            fmt.Println("Socket is closed - retrying")
            // Close the net sender channel because we'll reopen it later
            c.toSocket <- "../quit"
            c.controlChan <- "login"
            return
        }
        if e != nil {
            fmt.Println(e)
            os.Exit(1)
        }

// The `ragel` parser assumes we always have `<node>...</>` style trees
// but the first blob we get back is a leaf - `<edf="on"/>`.  Rather
// than try and work around this in the parser, we just special-case it
// and short-circuit.
        if bytes.Equal(buffer[:11], []byte("<edf=\"on\"/>")) {
//            fmt.Println("Shortcircuiting the initial leaf")
// FIXME
//            reqmap := edf.New_node("request", "user_login")
           // []string{"request=user_login", "name=xerox", "password=xerox", "status=256", "statusmsg=Logging in"}
 //           _ = reqmap
            login_req := fmt.Sprintf("<request=\"user_login\"><name=\"%s\"/><password=\"%s\"/><status=256/><statusmsg=\"Logging in\"/></>", c.username, c.password)
            c.toSocket<-login_req
            for i:=0; i<131072; i++ {
                buffer[i] = 0x0
            }
            continue
        }

// Ok, it's not the initiating leaf, let's try parsing it
        data = append(data, buffer[:n]...)

        trees, e, bytesParsed := edf.Parse(data)

        if bytesParsed > -1 {
// Print each of our trees
            bytesToSkip := 0
            for _, tree := range trees {
//                edf.As_tree(*tree)
                event_name := fmt.Sprintf("%s_%s", tree.Key, edf.InterfaceToString(tree.Value))
                var extra_info interface{}
                extra_info = nil
                event := ClientEvent{event_name, *tree, extra_info, &c}

                if event_name == "reply_folder_list" {
                   extra_info = c.ConstructFolderList(*tree)
                }
                if event_name == "announce_user_login" {
                    isItMe, e := event.Data.ValueOf("announce.username")
                    if e == nil {
                        if isItMe == c.Username() {
                            c.controlChan <- "loggedin"
                            c.Events <- ClientEvent{"_bot_login", edf.EDF_Tree{}, nil, &c}
                        }
                    }
                }
                c.Events<-event
                bytesToSkip = bytesToSkip + tree.Bytes
//                fmt.Printf("EDF: %s\n", string(data[:bytesToSkip]))
            }
            data = data[bytesToSkip:]
        }
        for i:=0; i<n; i++ {
            buffer[i] = 0x0
        }
    }
}

func debugging(ToPrint <-chan ClientEvent, wg *sync.WaitGroup) {
    defer wg.Done()
    for {
        out := <-ToPrint
        fmt.Printf("DB [%q]\n", out)
    }
}

func (c *Client) doConnect() {
	conn, err := net.Dial("tcp", "ua2.org:4040")
	if err != nil {
        fmt.Println(err)
        os.Exit(1)
	}

//    kaConn, _ := tcpkeepalive.EnableKeepAlive(conn)
//    kaConn.SetKeepAliveIdle(30*time.Second)
//    kaConn.SetKeepAliveCount(4)
//    kaConn.SetKeepAliveInterval(5*time.Second)

    c.conn = conn // kaConn
    c.controlChan <- "login"
}

func (c Client) RequestUserList(searchtype interface{}) {
    req := ""
    if searchtype == nil {
        req = "<request=\"user_list\"/>"
    } else {
        req = fmt.Sprintf("<request=\"user_list\"><searchtype=%d/></>", searchtype.(int))
    }
    c.toSocket<-string(req)
}

type FolderSub struct {
    Name string
    Id int
    Subscribed bool
}

func (c Client) SubscribeFolderById(folderid int) {
    req := fmt.Sprintf("<request=\"folder_subscribe\"><folderid=%d/></>", folderid)
    c.toSocket<-string(req)
}

func recurseFolderTree(tree edf.EDF_Tree, output *[]FolderSub) {
    folderChilds := tree.ChildrenMatch("folder")
    for _, child := range folderChilds {
        q := child.ValueMap()
        folderid, _ := strconv.Atoi(q["folder"])
        subtype, _ := strconv.Atoi(q["subtype"])
        *output = append(*output, FolderSub{q["name"],folderid, subtype == 1})
        if len(child.Children) > 0 {
            recurseFolderTree(child, output)
        }
    }
}

func (c Client) ConstructFolderList(tree edf.EDF_Tree) ([]FolderSub) {
    folders := []FolderSub{}
    childTrees := tree.ChildrenMatch("folder")
    for _, v := range childTrees {
        q := v.ValueMap()
        folderid, _ := strconv.Atoi(q["folder"])
        subtype, _ := strconv.Atoi(q["subtype"])
        folders = append(folders, FolderSub{q["name"],folderid,subtype==1})
        recurseFolderTree(v, &folders)
    }
    return folders
}

// All folders, low detail
func (c Client) RequestFolderList() {
    c.RequestFolderListConstrained(2)
}

func (c Client) RequestFolderListConstrained(searchtype int) {
    req := fmt.Sprintf("<request=\"folder_list\"><searchtype=%d/></>", searchtype)
    c.toSocket<-string(req)
}

func (c Client) RequestMessage(messageid int) {
    req := fmt.Sprintf("<request=\"message_list\"><messageid=%d/></>", messageid)
    c.toSocket<-string(req)
}

func (c Client) RequestWhoList() {
    c.RequestUserList(1)
}

func (c Client) doLogin() {
    // Start up the net listeners
    go c.net_out()
    go c.net_in()

    // fmt.Println("About to kick it all off")
    c.toSocket<-string("<edf=\"on\"/>")
    c.controlChan <- "running"
    c.Events <- ClientEvent{"_bot_running", edf.EDF_Tree{}, nil, &c}
}

func (c Client) handleControl() {
    defer c.wg.Done()
    c.wg.Add(1)

    for {
        v := <- c.controlChan
//        fmt.Println("Got control event:", v)

        switch v {
            case "startup": c.doConnect()
            case "login": c.doLogin()
        }
    }
}

func (c Client) SendControl(data string) {
    c.controlChan <- data
}

func New(username string, password string) (Client) {
    var wg sync.WaitGroup
    var ewg sync.WaitGroup

    // `output` is used to send to the socket.  Buffered but only mildly.
    output := make(chan string, 8)

    // `control` is used to pass bot control events around internally
    control := make(chan string, 8)

    // `events` is used to pass back to the client code.  We give this
    // a nice deep buffer to avoid blocking the socket whilst the higher
    // level client code is twiddling about.
    events := make(chan ClientEvent, 128)

//    fmt.Println("Creating a client for",username,"/",password)
    abschan := make(MapOfChannels)
    extramap := make(MapOfStrings)
    c := Client{username, password, nil, events, output, control, &wg, &ewg, abschan, extramap}

    return c
}

func (c Client)Username() (string) {
    return c.username
}

func (c Client)Login() {
    go c.handleControl()

    // It's safe to write to our control channel here because it's
    // buffered - even if the goroutine above hasn't started by the
    // time we write, it'll still get picked up once everything has
    // kicked itself off.
    c.controlChan <- "startup"

    c.wg.Wait()
    c.ExtraWG.Wait()

    for {
        command := <- c.controlChan
        if command == "quit" { return }
    }
}

func (c Client)Wait() {
    c.wg.Wait()
    c.ExtraWG.Wait()
}
