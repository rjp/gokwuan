# gokwuan

## A go client for UA2

Exactly what it says on the tin.  Rudimentary library for creating UA2 clients (a la the `uabot` Ruby) using a channel to pass events.

## Requirements

* [edf](https://bitbucket.org/rjp/edf)
* [ragel](http://www.colm.net/open-source/ragel/) (to build the parser)

## The Name

Entirely the fault of Mr TECHNO.